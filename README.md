# ESP32-S3-prodboard

ESP32 devboards meet (small-scale) production.

ESP32 devboards include USB-serial modules on each board while this could be on a separate module and re-used across devboards. This leads to:
* higher costs especially with the chip shortage which has sometimes made this more expensive than an ESP32 itself,
* higher power consumption since the chip isn't very good at saving power,
* wasted board space because the chip is fairly large and requires several external components.

ESP32-S3-prodboard has as few additional components as possible and maximal useful area while being easy to assemble by hand. It duplicates some GPIOs for use with buses such as SPI and I2C.

You should be able to get your projects close to mass-market costs and space usage along with good enough integration.

![3D render](./esp32-s3-prodboard.png)
